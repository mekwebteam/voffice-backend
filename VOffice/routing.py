from django.conf.urls import url

from channels.auth import AuthMiddlewareStack
#from channels.sessions import SessionMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
#import VOfficeApp.routing
from VOfficeApp import consumers
from VOfficeApp.token_auth import TokenAuthMiddlewareStack

application = ProtocolTypeRouter({

    "websocket": TokenAuthMiddlewareStack(
        URLRouter([
            url(r"^ws/office", consumers.OfficeConsumer),
        ])
    ),

})

