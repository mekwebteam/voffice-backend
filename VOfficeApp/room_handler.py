import logging

from VOfficeApp.submodels.room import Room
from VOfficeApp.submodels.company import Company
from VOfficeApp.submodels.userdata import UserData, Notification
from VOfficeApp.submodels.conversation import *
from VOfficeApp.models import User

from django.db.models.signals import post_save
from django.dispatch import receiver
from _datetime import datetime


logger = logging.getLogger(__name__) 
          
#we need to know when a new room was created, to add it to the mapping
@receiver(post_save, sender=Room)
def create_room(sender, instance, created, **kwargs):
    #if created:
    RoomHandler.instance.addRuntimeRoom(instance.id)

#todo each office(company, GroupdCompany) needs its own handler
class RoomHandler(object):
    
    instance = None
    
    def __init__(self):
                
        CallHandler.init()
        
        self.roomToUserMapping = dict()
        #Ready to send mapping of all users in this office
        self.userToRoomMapping = dict()
        
        #mapping users to websocket consumers       
        self.consumers = dict()
        #default room is the first reception we find
        self.default_room_id = -1        
        
        print("Init Room Manager")
                
        #gather all rooms at startup
        
        if Company.objects.count() == 0:
            return
        
        root_company = Company.objects.first() 
      
        if root_company is None:
            return 
        
        self.appendOffices(root_company)         
        self.appendRooms(root_company)    
    
        for b in root_company.branch_set.all(): 
            self.appendOffices(b)         
            self.appendRooms(b)  
        
            for d in b.department_set.all():
                self.addRuntimeRoom(d.room.id)
                
                
        #for u in UserData.objects.all():            
        #    if u.last_room is not None:
        #        
        #        slot_index = self.findFreeSlot(u.last_room_id)
        #        if slot_index < 0:
        #            continue
        
                #self.roomToUserMapping[u.last_room_id].append(u.user_id)
                #self.userToRoomMapping[u.user_id] = {'room': u.last_room_id, 'workspace': slot_index}
    
    @staticmethod
    def getSlots(room_id):
        
        if RoomHandler.instance is None or not room_id in RoomHandler.instance.roomToUserMapping:        
            return [] 
        
        return RoomHandler.instance.roomToUserMapping[room_id]
            
            
    def addRuntimeRoom(self, room_id):
        
        room = Room.objects.get(id=room_id)        
        
        if not room_id in self.roomToUserMapping:            
            self.roomToUserMapping[room_id] = []
            for i in range(0, room.size):
                self.roomToUserMapping[room_id].append(-1)
        
        else:
            old_mapping = self.roomToUserMapping[room_id]
            for i in range(0, room.size):
                if i < len(old_mapping):
                    self.roomToUserMapping[room_id].append(old_mapping[i])  
                else:
                    self.roomToUserMapping[room_id].append(-1)     
    
    def enterDefaultRoom(self, user_id):
        
        if self.default_room_id != -1:
            self.enterRoom(user_id, self.default_room_id, -1)            
    
    def enterWorkspace(self, user):
        
        if user.user_type != User.TYPE_EMPLOYEE:
            return False
         
        assignment = user.userdata.employee_data.assignment
        room_id = -1
        slot_index = 0
        
        if assignment is None:
            return False
        
        if not assignment.department is None:
            room_id = assignment.department.room_id
            slot_index = assignment.workspace
        else:
            if assignment.room is None:
                return False
            
            room_id = assignment.room_id
                
        self.enterRoom(user.id, room_id, slot_index)  
              
    def enterRoom(self, user_id, room_id, slots_index):
               
        if not room_id in self.roomToUserMapping:
            #should never happen
            
            logger.error("Room error <---")
            return False
        
        #leave previous room
        self.clearUserRoom(user_id) 
        
        room = Room.objects.get(id=room_id) 
        
        slot_offset = 0
        if room.room_type == Room.OFFICE_ROOM:
            slot_offset = 1
            if room.employeeassignment.employeedata.userdata.user_id == user_id:
                slot_offset = 0
            
        #find a free slot, if there is no fixed one, for other rooms than departments
        if slots_index == -1:
            slots_index = self.findFreeSlot(room_id, slot_offset)
            if slots_index == -1:
                return False
            
        if not self.setUserRoom(room_id, user_id, slots_index):
            return False        
        
        #broadcast update to all users   
        self.broadcast({'type' : 'update', 'update': 'roomStatus', 'roomStatus': {
            'enter': True,
            'room': room_id, 
            'user': user_id, 
            'slot': slots_index 
        }})
        
        #write room in database
        user = User.objects.get(id=user_id)
        user.userdata.last_room = Room.objects.get(id=room_id)
        user.userdata.save()
              
        
        return True
        
    def clearUserRoom(self, user_id):
        
        if not user_id in self.userToRoomMapping or self.userToRoomMapping[user_id] == -1:
            return
        
        room_id = self.userToRoomMapping[user_id]
        
        old_slot = self.clearUserSlot(user_id, room_id)
        
        if old_slot == -1:     
            return 
        
        self.userToRoomMapping[user_id] = -1
        
        self.broadcast({'type' : 'update', 'update': 'roomStatus', 'roomStatus': {
            'enter': False,
            'room': room_id, 
            'slot': old_slot 
        }})    
    
    def clearUserSlot(self, user_id, room_id):
        
        roomMapping = self.roomToUserMapping[room_id]
        
        for i in range(0, len(roomMapping)):
            if(roomMapping[i] == user_id):
                
                roomMapping[i] = -1                
                return i
        
        return -1
                
    def setUserRoom(self, room_id, user_id, slot_index):
        
        roomMapping = self.roomToUserMapping[room_id]
            
        roomMapping[slot_index] = user_id
        self.userToRoomMapping[user_id] = room_id
        
        return True
    def findFreeSlot(self, room_id, slot_offset):
        
        roomMapping = self.roomToUserMapping[room_id]
        
        for i in range(slot_offset, len(roomMapping)):
            if(roomMapping[i] == -1):
                              
                return i        
        
        return -1    
        
    
    #register user when he connects and was verified
    def registerUser(self, user_id, consumer):  
        
        logger.info("User connected %i" % user_id )
        
        user = User.objects.get(id=user_id)
        self.enterWorkspace(user)
        
        #self.enterDefaultRoom(user_id)
        
        #check if user already has a connection, from another tab or window
        if user_id in self.consumers:
            logger.info("Remove other window connection %i" % user_id )
            #if yes close it and signal not to reconnect
            self.consumers[user_id].send_json({'type' : 'close' })
            self.consumers[user_id].close()
            self.consumers.pop(user_id, None) 
        
        
        self.broadcastUserStatus(user_id, True)
        self.consumers[user_id] = consumer 
        
    
        
    #unregister a user when his connection was closed        
    def unregisterUser(self, user_id):        
        
        old_consumer = self.consumers.pop(user_id, None) 
        if old_consumer is not None:
            old_consumer.close()
            
        
        self.clearUserRoom(user_id)
        CallHandler.instance.leaveCall(user_id)
        
        self.broadcastUserStatus(user_id, False)
    
    def isConnected(self, user_id):
        return user_id in self.consumers
    
    def send(self, user_id, data):
        
        if user_id in self.consumers:
            self.consumers[user_id].send_json(data)
            
            return True
        
        return False
    #broadcast a message to all users in the office
    def broadcast(self, data):
        
        temp_consumers = self.consumers.copy()
        for consumer in temp_consumers.values():
            consumer.send_json(data)
            
    def broadcastExcluded(self, data, exclude):
        
        temp_consumers = self.consumers.copy()
        for consumer in temp_consumers.values():
            if consumer != exclude:
                consumer.send_json(data)
                
    def broadcastOfficeUpdate(self):
        self.broadcast({'type' : 'update' , 'update' : 'office'})
        
    def broadcastUserStatus(self, user_id, is_online):
        self.broadcast({'type' : 'update' , 'update' : 'status', 'status' : {'user' : user_id, 'is_online' : is_online}})
    
    #for gathering users offices at init
    def appendOffices(self, level):
        for u in level.employeeassignment_set.all():
            self.addRuntimeRoom(u.room_id)
            
    #for gathering rooms at init   
    def appendRooms(self, level):
        for room in level.rooms.all():   
            if self.default_room_id == -1 and room.room_type == Room.RECEPTION_ROOM:
                self.default_room_id = room.id
            self.addRuntimeRoom(room.id)
                
    
    
            
    @staticmethod
    def init():
        if RoomHandler.instance == None:
            RoomHandler.instance = RoomHandler()
        
    
    @staticmethod
    def maxY(level):
        
        y = 0
        for room in level.rooms.all():    
            y = max([y, room.y + room.height])
        
        return y
    
    @staticmethod
    def findfreeRoomPosition():
        
        root_company = Company.objects.first()
        
        if root_company is None:        
            return 0
            
        y = max([0, RoomHandler.maxY(root_company)])
            
        for b in root_company.branch_set.all():
            
            y = max([y, RoomHandler.maxY(b)])
            
            for d in b.department_set.all():
                
                y = max([y, d.room.y + d.room.height])
                
        return y

class CallHandler(object):
    
    instance = None
    
    def __init__(self):
        
        self.id_counter = 0
        self.active_calls = dict()
        self.call_to_user = dict()
    
    
    def addUserToCall(self, user_id, call_id):
        
        self.active_calls[call_id]['users'].append(user_id)
        #self.active_calls[call_id]['all_users'].append(user_id)
        self.call_to_user[user_id] = call_id
            
    def removeUserFromCall(self, user_id, call_id):        
        
        self.active_calls[call_id]['users'].remove(user_id)
        del self.call_to_user[user_id]
                
    def isInCall(self, user_id):
        return user_id in self.call_to_user
    def isCallValid(self, call_id):
        return call_id in self.active_calls;
    
    def callUser(self, user, target_user, conversation):
        
        if self.isInCall(user.id):
            return -1
        if self.isInCall(target_user.id):
            return -1
        
        call_message = self.createCallMessage(user, conversation)
        #Call user if he is online
        if RoomHandler.instance.isConnected(target_user.id):
            
            call = CallId.objects.create(message=call_message)
                        
            self.active_calls[call.id] = { 'users' : [], 'all_users': [user.id, target_user.id]}
            self.addUserToCall(user.id, call.id)
            
            RoomHandler.instance.send(target_user.id, {'type' : 'call' , 'call' : {
                'id' : call.id, 'user' : user.id, 'conversation': conversation.id, 'type': 'offer'}})
            
            return call.id           
            
        #Create missed call notification    
        new_notification = Notification.objects.create(user=target_user, 
          notification_type=Notification.TYPE_MISSED_CALL, target_id=conversation.id, source_id=user.id)
        new_notification.save()
        
        return -1
    
    def joinCall(self, user_id, call_id):
        
        if self.isInCall(user_id):
            logger.error("User tries to join another call %i,call_id: %i" % (user_id, call_id))            
            return False
        
        if not self.isCallValid(call_id):
            logger.error("Call join error %i,call_id: %i" % (user_id, call_id))        
            return False
        
        call = CallId.objects.get(id=call_id) 
        if not call.was_active:
            call.was_active = True
            call.save()
            
        self.addUserToCall(user_id, call_id)
        
        return True
        
    def createCallMessage(self, source_user, conversation):
        message = Message.objects.create(sender=source_user.id, text='Call', conversation=conversation, 
                                         message_type=Message.TYPE_CALL)
        message.save()        
        
        from VOfficeApp.subviews.conversation import updateMessage
        updateMessage(conversation, message, source_user)
        
        return message
    
    #send by called uer to decline a call  
    def declineCall(self, source_user_id, call):
         
        if not self.isCallValid(call.id):
            logger.error("Call decline error %i,call_id: %i" % (source_user_id, call.id))        
            return
        
        self.active_calls[call.id]['all_users'].remove(source_user_id)
        
        for u in self.active_calls[call.id]['users']:
            RoomHandler.instance.send(u, {'type' : 'call' , 'call' : {
                'user': source_user_id , 'id' : call.id, 'type': 'decline'}})
                
        self.closeCall(call);
        
    def leaveCall(self, user_id):  
        
        if not self.isInCall(user_id):
            #logger.info("Leaving user not in call %i" % (user_id))
            return
        
        call_id = self.call_to_user[user_id]
        
        if not self.isCallValid(call_id):
            logger.error("Call leave error %i,call_id: %i" % (user_id, call_id))        
            return
        
        call = CallId.objects.get(id=call_id)
        
        self.removeUserFromCall(user_id, call_id)
        
        self.closeCall(call);
    
    def closeCall(self, call):
        
        if len(self.active_calls[call.id]['users']) > 1:
            return
        
        call.end_date = datetime.now()
        call.save()
        
        print("Call ended %i" % call.id )
        logger.info("Call ended %i" % call.id )
        
        for u in self.active_calls[call.id]['users']:
            del self.call_to_user[u]
            
        for u in self.active_calls[call.id]['all_users']:
        
            RoomHandler.instance.send(u, {'type' : 'call' , 'call' : {
                #'user': source_user_id , 
                'id' : call.id , 
                'conversation' : call.message.conversation_id, 
                'end_date': str(call.end_date), 
                'type': 'end'
            }})
            
        del self.active_calls[call.id]
    """
    def nextCallId(self):
        self.id_counter += 1
        
        return self.id_counter
    
    def createCall(self, title):
        
        print("Create Call")       
      
            
    def joinCall(self, conference_id, user, user_consumer):        
       
        conference = Conference.objects.get(id=conference_id)
        
        if conference.id not in self.active_calls:
            self.active_calls[conference.id] = { 
                'participants':[] 
            }
            
                                    
        user_consumer.send_json({
            'type' : 'signal',
            'signal': 'enter',
            'id': conference_id,
            'users' : self.active_calls[conference_id]['participants']
        })          
        
        for other_user in self.active_calls[conference_id]['participants']:            
                    
            RoomHandler.instance.consumers[other_user].send_json({
                'type' : 'signal',
                'signal': 'join',
                'user' : user.id
            })
            
                    
        self.active_calls[conference_id]['participants'].append(user.id)
       
        
        logger.info("User joined %i" % user.id)
    
    def leaveCall(self,  user_id):      
        
        #print("start leave")
        
        logger.info("user leaving %i" % user_id)
                        
        for call in self.active_calls.values():            
        
            if self.removeUser(call['participants'], user_id):
                for u in call['participants']:
                    RoomHandler.instance.send(u, {
                        'type' : 'signal',
                        'signal': 'leave',
                        'user' : user_id
                    })
                 
                logger.info(call)   
                logger.info("user left %i" % user_id)
                #break
            
       
    def removeUser(self, participants, user_id):
                 
        for u in range(len(participants)):
            if participants[u] == user_id:
                participants.pop(u)
                
                return True
            
        return False
    """ 
    @staticmethod
    def init():
        if CallHandler.instance == None:
            CallHandler.instance = CallHandler()