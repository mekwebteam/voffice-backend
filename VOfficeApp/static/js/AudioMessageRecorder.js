var AudioMessageRecorder = function() {

	var self = this;
	
	this.mediaRecorder = null;
	this.recordedBlobs = [];
	this.buffer = null;
	//this.audioElement = inAudioElement;
	
	this.record = function(availableEvent , failEvent) {
		
		const constraints = {
			audio: {
			//echoCancellation: {exact: hasEchoCancellation}
			},
			video: false
		};
		
		navigator.mediaDevices.getUserMedia(constraints).then(function (stream){			
		
			self.mediaRecorder = new MediaRecorder(stream);
			
			self.mediaRecorder.onstop = (event) => {
			    

				console.log(self.recordedBlobs);
				if(self.recordedBlobs.length <= 0) {
					return;
				}
				self.buffer = new Blob(self.recordedBlobs, { 'type' : self.recordedBlobs[0].type });

				console.log(self.recordedBlobs[0].type);
				availableEvent();
				/*var recordedVideo = $("#" + self.audioElement)[0];
				
				recordedVideo.src = window.URL.createObjectURL(self.superBuffer);
				recordedVideo.controls = true;
				recordedVideo.play();*/
			};
			
			self.mediaRecorder.ondataavailable = self.handleDataAvailable;
			self.mediaRecorder.start();
		
		}).catch(failEvent);
	}
	this.handleDataAvailable = function(event) {
		
		if (event.data && event.data.size > 0) {
			self.recordedBlobs.push(event.data);
		}
	}
	this.stop = function() {
		self.mediaRecorder.stop();

		console.log("stop");
	}
	
	
}