
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from VOfficeApp.models import User
from VOfficeApp.submodels.company import EmployeeAssignment, Department, Branch,\
    GroupCompany, Company
from VOfficeApp.room_handler import RoomHandler
from VOfficeApp.submodels.room import Room

def user_to_json(user):
    return {
        'id': user.id,
        #'email': user.email,
        'firstname': user.first_name,
        'lastname': user.last_name,
        'designation': user.designation_id,
        'is_online': RoomHandler.instance.isConnected(user.id)
    }


def findFreeDepartmentSlot(assignemt):
    
    
    department = assignemt.department
    slot_set = set()
        
    num_used_slots = department.employeeassignment_set.count()
    
    max_size = department.room.size
       
    if num_used_slots == max_size:

        if department.room.width < 5:
            department.room.width += 1
            department.room.size += 1
        else:    
            department.room.size += (department.room.width - 1)
            department.room.height += 1
        department.room.save()
        department.save()
                                
        return max_size
    else:
        for a in department.employeeassignment_set.all():
            slot_set.add(a.workspace)         
            
        for s in range(max_size):
            if not s in slot_set:                
                return s   

    #should never happen
    return -1

#clean old assignemnt but keep the assigned room
def cleanAssignemt(user, assignemt):
    
    #in case user was in his departmen when assigning a level
    if assignemt.department is not None and assignemt.department.room == user.userdata.last_room:
        RoomHandler.instance.enterDefaultRoom(user.id)
    
    
    assignemt.department = None
    assignemt.branch = None
    assignemt.group_company = None
    assignemt.company = None
   
    assignemt.workspace = -1
  

class AssignUserView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        user_id = request.data.get('user')
        level_type = request.data.get('type')
        level_id = request.data.get('level')
       

        if assignUserToLevel(user_id, level_type, level_id):
            return Response() 
        
        return Response(status=404) 
              
'''
@require_http_methods(["POST"])   
def assignUser(request):    
    
    jsonData = json.loads(request.POST['data'])
   
    user_id = jsonData['user']
    level_type = jsonData['type']
    level_id = jsonData['level']

    if assignUserToLevel(user_id, level_type, level_id):
        HttpResponse(status=200) 
        
    return HttpResponse(status=500) 
'''
def assignUserToLevel(user_id, level_type, level_id):
            
    user = User.objects.get(id=user_id)
    employee_data = user.userdata.employee_data
    
    if employee_data is None:
        return False
       
    assignemt = None
    
    #first check if user was already assigned
    if employee_data.assignment is not None:
        assignemt = employee_data.assignment
        #cleanup all previous assignemnt, but keep room
        cleanAssignemt(user, assignemt)
    else:
        assignemt = EmployeeAssignment.objects.create()            
        employee_data.assignment = assignemt
    
    level = None
    
    if level_type == 'department':
        level = Department.objects.get(id=level_id)
        assignemt.department = level
        
        #assign free workspace   
        assignemt.workspace = findFreeDepartmentSlot(assignemt)
    elif level_type == 'branch':
        level = Branch.objects.get(id=level_id)
        assignemt.branch = level
        
    elif level_type == 'group':
        level = GroupCompany.objects.get(id=level_id)
        assignemt.group_company = level
        
    elif level_type == 'company':
        level = Company.objects.first()
        assignemt.company = level
    else:
        return False
    
    #create an office for everyone not assigned to a department
    if level_type != 'department':
        if assignemt.room is None:
            room_y = RoomHandler.findfreeRoomPosition()
            assignemt.room = Room.objects.create(name='Office', room_type=Room.OFFICE_ROOM, width=3, height=3, size=3, y=room_y, x=0)
            assignemt.room.save()
    else:
        #remove old office if user was assigned to a department
        if assignemt.room is not None:
            assignemt.room.delete()
            assignemt.room = None
            
    level.save()
    assignemt.save()
    employee_data.save()
        
    RoomHandler.instance.broadcastOfficeUpdate();
    
    return True 




class UpdateUserDataView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        firstname = request.data.get('firstname')
        lastname = request.data.get('lastname')
        department_room_id = request.data.get('department')
        email = request.data.get('email')
        
        if request.user.user_type != User.TYPE_EMPLOYEE:
            return Response(status=404)
        
        request.user.first_name = firstname
        request.user.last_name = lastname
        request.user.email = email
        request.user.save()
        
        target_department = Department.objects.get(room_id=department_room_id)
            
        assignment = request.user.userdata.employee_data.assignment       
        
        print(target_department.id)
        if not assignment.department is None:
            if assignment.department.id != target_department.id:
                assignUserToLevel(request.user.id, 'department', target_department.id)
                    
        
        
        RoomHandler.instance.broadcast({'type' : 'update' , 'update' : 'user', 'user' : user_to_json(request.user)})
        
        return Response()