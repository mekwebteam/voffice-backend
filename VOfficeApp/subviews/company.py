#from django.shortcuts import render

#from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.db.models import Q
#from django.contrib.auth.models import Permission
#from django.core.exceptions import * 
import json

from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission

from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import routers, serializers, viewsets

from VOfficeApp.models import User, RoleGroup, Designation, office_permissions
#from VOfficeApp.submodels.userdata import UserData
from VOfficeApp.submodels.company import Company, GroupCompany, Branch, Department, Team
from VOfficeApp.submodels.room import Room
from VOfficeApp.room_handler import RoomHandler

from rest_framework import generics, status
from drf_yasg import openapi
from drf_yasg.inspectors import SwaggerAutoSchema
from drf_yasg.utils import swagger_auto_schema

def appendBranches(root, branch_list):
    
    for b in root.all():        
        branch_data = {
            'id':b.id,
            'name': b.name,
            'departments': [],
            'users': []
        }  
        
        appendUsers(b.employeeassignment_set, branch_data['users'])  
            
        for d in b.department_set.all():            
            department_data = {
                'id':d.id,
                'name': d.name,
                'users': []
            }  
            
            appendUsers(d.employeeassignment_set, department_data['users'])  
            
            branch_data['departments'].append(department_data);
            
        branch_list.append(branch_data)



def createDepartment(name, parent):
    
    
    new_dep = Department.objects.create(name=name, branch=parent)
    new_dep.room = Room.objects.create(name=name, room_type=Room.DEPARTMENT_ROOM, room_template=None, width=3, height=2, size=2)
    new_dep.room.save()
    new_dep.save()
    
    return new_dep
'''
'''
#
class AddDepartmentView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='parent', in_=openapi.IN_HEADER,
                type=openapi.TYPE_INTEGER,
                description="ID of the parent branch",
                required=True
            ),
            openapi.Parameter(
                name='name', in_=openapi.IN_HEADER,
                type=openapi.TYPE_STRING,
                description="Name of the new Department",
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="Department was created"
            )
        }
    )
    def post(self, request):        
        """
        Adds a new Department beneath a Branch
        """
        
        parent_id = request.data.get('parent')
        name = request.data.get('name')
   
        parent = Branch.objects.get(id=parent_id)
        new_level = createDepartment(name, parent)
        new_level.save()
        
        return Response() 

class AddBranchView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='parent', in_=openapi.IN_HEADER,
                type=openapi.TYPE_INTEGER,
                description="ID of the parent company",
                required=True
            ),
            openapi.Parameter(
                name='name', in_=openapi.IN_HEADER,
                type=openapi.TYPE_STRING,
                description="Name of the new Branch",
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="Branch was created"
            )
        }
    )
    def post(self, request):        
        """
        Adds a new Branch beneath a Branch
        """
        parent_id = request.data.get('parent')
        name = request.data.get('name')
   
        parent = Company.objects.get(id=parent_id)
        new_level = Branch.objects.create(name=name, company=parent)
        new_level.save()
        
        return Response() 
    
'''    
    if level_type == 'branch':
        parent = Branch.objects.get(id=parent_id)
        new_level = createDepartment(name, parent)
    elif level_type == 'group':
        parent = GroupCompany.objects.get(id=parent_id)
        new_level = Branch.objects.create(name=name, group_company=parent)
    elif level_type == 'company':
        is_group = jsonData['isGroup']
        parent = Company.objects.first()
        if is_group:
            new_level = GroupCompany.objects.create(name=name, company=parent)
        else:
            new_level = Branch.objects.create(name=name, company=parent)
            
    new_level.save()        
    
    return HttpResponse(status=200)  
'''
def appendUsers(root_list, user_list):
    
    for u in root_list.all():  
        user =  u.employeedata.userdata.user     
        user_list.append(user.id)
            #{
            #'id':user.id,
            #'email': user.email
        #})
   


class GetCompanyView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):  
    
        answer = { 
            'name' : '',   
            'users': [],
            'groups' : [],
            'branches' : [],
            'not_assigned': []
        }
        
        root_company = Company.objects.first()
        if root_company == None:        
            return JsonResponse(answer)
        
        answer['name'] = root_company.name;
          
        appendUsers(root_company.employeeassignment_set, answer['users'])    
        
        for g in root_company.groupcompany_set.all():
            group_data = {
                'id':g.id,
                'name': g.name,
                'branches': [],
                'users': []
            }
            appendUsers(g.employeeassignment_set,group_data['users'])  
            appendBranches(g.branch_set, group_data['branches'])
            
            answer['groups'].append(group_data)
             
        appendBranches(root_company.branch_set, answer['branches'])
      
        for u in User.objects.filter(Q(user_type='e'), Q(userdata__employee_data__assignment__isnull=True)):        
            answer['not_assigned'].append(u.id)  
            
        return Response(answer)

class DesignationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Designation
        fields = ['id', 'name', 'description']

class DesignationViewSet(viewsets.ModelViewSet):
    queryset = Designation.objects.all()
    serializer_class = DesignationSerializer

#@require_http_methods(["GET"])   
def getPermissionData(request):
        
     
    answer = {        
        'designations': [],
        'roles': [],
        'permissions': [],
    }
    for p, desc in office_permissions:
        print(p)
        answer['permissions'].append({
            'description': desc,
            'id': p
    })
        
      
    for g in  RoleGroup.objects.all():
        
        role = {
            'id': g.id,
            'name': g.group.name,
            'description': g.description,
            'permissions': []
        }
        
        group = g.group
        
        for p in group.permissions.all():
            role['permissions'].append({
                'description': p.name,
                'id': p.codename
            })
        
        answer['roles'].append(role)
    
        
    for d in Designation.objects.all():
        designation = {
            'id': d.id,
            'name': d.name,
            'description': d.description,
            'roles': []
        }
        for r in d.roles.all():
            designation['roles'].append({
                'name': r.group.name,
                'description': r.description,
                'id': r.id
            })
            
        answer['designations'].append(designation)
        
    return JsonResponse(answer)

@require_http_methods(["POST"])   
def addRole(request):
    
    jsonData = json.loads(request.POST['data'])
   
    name = jsonData['name']
    description = jsonData['description']
    
    if len(name) <= 0:
        return HttpResponse(status=500)   
    
    if Group.objects.filter(name=name).exists():
        return HttpResponse(status=500)   
    
    
    group = Group.objects.create(name=name)
    group.save()
    
    new_role = RoleGroup.objects.create(group=group, description=description)  
    new_role.save()
      
    return HttpResponse(status=200)

@require_http_methods(["POST"])   
def assignRolePermission(request):
    
    jsonData = json.loads(request.POST['data'])
   
    role_id = jsonData['role']
    permissionName = jsonData['permission']    
   
    if len(permissionName) <= 0:
        return HttpResponse(status=500)    
    
    roleGroup = RoleGroup.objects.get(id=role_id)
  
    permission = Permission.objects.get(codename=permissionName)    
    roleGroup.group.permissions.add(permission)  
      
    return HttpResponse(status=200)

@require_http_methods(["POST"])   
def removeRolePermission(request):
    
    jsonData = json.loads(request.POST['data'])
   
    role_id = jsonData['role']
    permission_id = jsonData['permission']
       
    if len(permission_id) <= 0:
        return HttpResponse(status=404)   
    
    
    role_group = RoleGroup.objects.get(id=role_id)
  
    permission = Permission.objects.get(codename=permission_id)    
    role_group.group.permissions.remove(permission)  
      
    return HttpResponse(status=200)

'''
@require_http_methods(["POST"])   
def addDesignation(request):
    
    jsonData = json.loads(request.POST['data'])
   
    name = jsonData['name']
    description = jsonData['description']
    
    if len(name) <= 0:
        return HttpResponse(status=500) 
      
    
    new_designation = Designation.objects.create(name=name, description=description)  
    new_designation.save()
      
    return HttpResponse(status=200)
'''

class AddRoleToDesignationView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='role', in_=openapi.IN_HEADER,
                type=openapi.TYPE_INTEGER,
                description="ID of the Role",
                required=True
            ),
            openapi.Parameter(
                name='designation', in_=openapi.IN_HEADER,
                type=openapi.TYPE_STRING,
                description="ID of the Designation",
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description=""
            )
        }
    )
    def post(self, request):
        """
        Adds a Role to a Designation
        """   
        
        role_id = request.data.get('role')
        designation_id = request.data.get('designation')       
       
        role_group = RoleGroup.objects.get(id=role_id)
        designation = Designation.objects.get(id=designation_id)
        
        designation.roles.add(role_group)
        designation.save()
          
        return Response(status=200)



class RemoveRoleToDesignationView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='role', in_=openapi.IN_HEADER,
                type=openapi.TYPE_INTEGER,
                description="ID of the Role",
                required=True
            ),
            openapi.Parameter(
                name='designation', in_=openapi.IN_HEADER,
                type=openapi.TYPE_STRING,
                description="ID of the Designation",
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description=""
            )
        }
    )
    def post(self, request):
        """
        Removes a Role from a Designation
        """
    
        role_id = request.data.get('role')
        designation_id = request.data.get('designation')   
        
           
    
        role_group = RoleGroup.objects.get(id=role_id)  
        designation = Designation.objects.get(id=designation_id)
        
        designation.roles.remove(role_group)  
      
        return Response(status=200)


class AssignUserDesignationView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='user', in_=openapi.IN_HEADER,
                type=openapi.TYPE_INTEGER,
                description="ID of the User",
                required=True
            ),
            openapi.Parameter(
                name='designation', in_=openapi.IN_HEADER,
                type=openapi.TYPE_STRING,
                description="ID of the Designation",
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description=""
            )
        }
    )
    def post(self, request):        
        """
        Assign a Designation to an User
        """
        user_id = request.data.get('user')
        designation_id = int(request.data.get('designation'))
       
        user = User.objects.get(id=user_id)
        
        if user.designation is not None:
            for d in user.designation.roles.all():
                user.groups.remove(d.group)    
        
        if designation_id == -1:
            user.designation = None
            user.save()
            return Response(status=200)
        
        designation = Designation.objects.get(id=designation_id)
        
        user.designation = designation    
        
        for d in designation.roles.all():
            user.groups.add(d.group)
        
        user.save()
          
        return Response(status=200)

#@login_required()
@require_http_methods(["POST"])   
def createTestCompany(request):
    
    root_company = Company.objects.create(name='company')
    root_company.save()
    
    #group = GroupCompany.objects.create(name='group', company=root_company)
    #group.save()
    
    branch1 = Branch.objects.create(name='branch1', company=root_company) 
    branch1.save()
    
    branch2 = Branch.objects.create(name='branch2', company=root_company) 
    branch2.save()
    
    
    createDepartment('department1', branch1)
    createDepartment('department2', branch1)
    createDepartment('department3', branch1)
    
    createDepartment('department4', branch2)
    createDepartment('department5', branch2)
    createDepartment('department6', branch2)
            
    return HttpResponse(status=200)  
    
    
class AddTeamView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='name', in_=openapi.IN_HEADER,
                type=openapi.TYPE_STRING,
                description="Name of the team",
                required=True
            ),
            openapi.Parameter(
                name='members', in_=openapi.IN_HEADER,
                type=openapi.TYPE_ARRAY,
                items=openapi.Items(type=openapi.TYPE_INTEGER),
                description="List of User IDs",
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="Team was created"
            )
        }
    )
    def post(self, request):
        """
        Add a new Team
        """
        name = request.data.get('name')
        members = request.data.get('members')
        
        new_team = Team.objects.create(creator=request.user, name=name)
        
        for member in members:
            user = User.objects.get(id=member)
            new_team.members.add(user)
        
        new_team.save()
        
        RoomHandler.instance.broadcast({'type' : 'update', 'update': 'team', 'team': team_to_json(new_team)})
        
        return Response() 
    
class DeleteTeamView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='team', in_=openapi.IN_HEADER,
                type=openapi.TYPE_INTEGER,
                description="ID of the Team",
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="Team was deleted"
            )
        }
    )
    def post(self, request):
        """
        Delete a Team
        """
        team_id = request.data.get('team')
        
        team = Team.objects.get(id=team_id)
        
        if team.creator.id != request.user.id:
            return Response(status=404)
        team.delete()
        
        RoomHandler.instance.broadcast({'type' : 'update', 'update': 'teamDelete', 'teamDelete': team_id})
        
        return Response()     

class AssignToTeamView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='user', in_=openapi.IN_HEADER,
                type=openapi.TYPE_INTEGER,
                description="ID of the User",
                required=True
            ),
            openapi.Parameter(
                name='team', in_=openapi.IN_HEADER,
                type=openapi.TYPE_INTEGER,
                description="ID of the Team",
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="User was assigned"
            ),       
            status.HTTP_404_NOT_FOUND: openapi.Response(
                description="Only team creator can assign users"
            )
        }
    )
    def post(self, request):
        """
        Assign a User to a Team
        """
        user_id = request.data.get('user')
        team_id = request.data.get('team')
        
        team = Team.objects.get(id=team_id)
        
        if team.creator.id != request.user.id:
            return Response(status=404)
         
        user = User.objects.get(id=user_id)
        team.members.add(user)
        
        team.save()
        
        RoomHandler.instance.broadcast({'type' : 'update', 'update': 'team', 'team': team_to_json(team)})
        
        return Response() 
       
class RemoveFromTeamView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='user', in_=openapi.IN_HEADER,
                type=openapi.TYPE_INTEGER,
                description="ID of the User",
                required=True
            ),
            openapi.Parameter(
                name='team', in_=openapi.IN_HEADER,
                type=openapi.TYPE_INTEGER,
                description="ID of the Team",
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="User was removed"
            ),       
            status.HTTP_404_NOT_FOUND: openapi.Response(
                description="Only team creator can remove users"
            )
        }
    )
    def post(self, request):
        """
        Remove a User from a team
        """
        user_id = request.data.get('user')
        team_id = request.data.get('team')
        
        team = Team.objects.get(id=team_id)
        
        if team.creator.id != request.user.id:
            return Response(status=404)
        
        user = User.objects.get(id=user_id)
        team.members.remove(user)
        
        team.save()
        
        RoomHandler.instance.broadcast({'type' : 'update', 'update': 'team', 'team': team_to_json(team)})
        
        return Response()

def addTeamMember(departments, member):
    if member.user_type != User.TYPE_EMPLOYEE:
            return        
    if member.userdata is None or member.userdata.employee_data is None:
        return
              
    assignment = member.userdata.employee_data.assignment
    
    if assignment is None:
        return
              
    if not assignment.department is None:            
        
        if not assignment.department.id in departments:
            departments[assignment.department.id] = {
                'id': assignment.department.id,
                'name' :assignment.department.name,
                'users': []
            }
        departments[assignment.department.id]['users'].append(member.id)
                
def team_to_json(team):
    
    departments = {}
    
    members = []
    
    #addTeamMember(departments, team.creator)
            
    for member in team.members.all():
                
        addTeamMember(departments, member)
        
        members.append(member.id)
        
        
    return {
        'id' : team.id,
        'name' : team.name,
        'creator': team.creator_id,
        'members': members,
        'departments' : departments
    }
         
class ListTeamsView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        answer = {}
        for team in Team.objects.all():
                            
            answer[team.id] = team_to_json(team)
        
        return Response(answer)