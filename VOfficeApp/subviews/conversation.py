
from django.http import HttpResponse, JsonResponse, FileResponse
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.db.models import Q

#from django.contrib.auth.models import Permission
#from django.core.exceptions import * 
import json

import base64
from django.utils.dateparse import parse_datetime
from datetime import datetime

from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from VOfficeApp.models import User
from VOfficeApp.submodels.conversation import *
from VOfficeApp.submodels.files import createFileEntry, FileShareUsers

from VOfficeApp.room_handler import RoomHandler, CallHandler
from VOfficeApp.submodels.userdata import Notification
from VOfficeApp.submodels.room import Room
    
class AddConversationView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
     
        topic = request.data.get('topic')    
        target_ids = request.data.get('users')
                           
        conversation = Conversation.objects.create(topic=topic)     
        
        conversation.participants.add(request.user)
        
        for u in target_ids:
            target_user = User.objects.get(id=u)       
            conversation.participants.add(target_user)
       
        conversation.save()
        
        conversation_json = conversation_to_json(request.user, conversation)
        
        for u in target_ids:
            RoomHandler.instance.send(u, { 'type': 'update', 'update': 'conversation', 'conversation': conversation_json})
        
        return Response(conversation_json)

class AddToConversationView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):     
     
        conversation_id = request.data.get('conversation')
        target_user_id = request.data.get('user')
                
        target_user = User.objects.get(id=target_user_id)
        conversation = Conversation.objects.get(id=conversation_id)          
        
        conversation.participants.add(target_user)
        conversation.save()        
        
        conversation_json = conversation_to_json(None , conversation)
        
        for user in conversation.participants.all():
        
            RoomHandler.instance.send(user.id, { 'type': 'update', 'update': 'conversation', 'conversation': conversation_json})
        
        return Response()


class RequestConversationView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request, conversation_id):
        
        answer = []    
        conversation = Conversation.objects.get(id=conversation_id) 
        
        for message in conversation.messages.order_by('date'):
            
            message_data = message.to_json()
               
            has_read_flag = NotRead.objects.filter(message=message.id, user=request.user.id)
           
            if has_read_flag.exists():
                message_data['new_message'] = True                             
                has_read_flag.delete()
           
            answer.append(message_data) 
            
        return Response(answer)

    
class SendMessageView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
     
        conversation_id = request.data.get('conversation')
        message_text = request.data.get('text')
        
        if len(message_text) == 0:        
            return Response(status=404)
            
        
        conversation = Conversation.objects.get(id=conversation_id)     
        
        message = Message.objects.create(sender=request.user.id, text=message_text, conversation=conversation)
        message.save()
        
        updateMessage(conversation, message, request.user)
        
        return Response()
    
    
class SendFileMessageView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        conversation_id = int(request.POST['conversation'])
        
        conversation = Conversation.objects.get(id=conversation_id)   
        
        file = createFileEntry(request)
        file.shared_users = FileShareUsers.objects.create()
       
        
        for other_user in conversation.participants.all():
            if other_user != request.user:
                file.shared_users.users.add(other_user)
        
        message = Message.objects.create(sender=request.user.id, text=file.name, 
            conversation=conversation, message_type=Message.TYPE_FILE, data_id=file.id)
        
        message.save()
        file.shared_users.save()  
        file.save()  
        
        updateMessage(conversation, message, request.user)
        
        return Response()  

class SendAudioMessageView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
       
        conversation_id = request.POST['conversation']
        conversation = Conversation.objects.get(id=conversation_id)
        
        audio_message = AudioMessage.objects.create(audio_data=request.FILES['audio'])
        audio_message.save();           
        
        message = Message.objects.create(sender=request.user.id, text='audio', 
            conversation=conversation, message_type=Message.TYPE_AUDIO, data_id=audio_message.id)
        message.save()
           
        updateMessage(conversation, message, request.user)
        
        return HttpResponse()  
                    
def conversation_to_json(user, conversation): 
    if conversation is None:
        return None
    
    num_unread_messages = 0
    
    if not user is None:
        num_unread_messages = NotRead.objects.filter(user=user.id, message__conversation__id=conversation.id ).count()    
       
    conversation_data = {
            'id' : conversation.id,
            'topic' : conversation.topic,
            'is_conference' : conversation.is_conference,
            'users' : [],
            'unread': num_unread_messages,
            'latest' : None
    }
    for user in conversation.participants.all():
        conversation_data['users'].append(user.id)
    
    if conversation.latest_message > -1:
        conversation_data['latest'] = Message.objects.get(id=conversation.latest_message).to_json()
        
    return conversation_data

class ListConversationsView(APIView):
    
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        answer = []
        
        for conversation in request.user.conversations.filter(is_conference=False):
                             
            answer.append(conversation_to_json(request.user, conversation))        
               
        return Response(answer)

class GetMessageStatusView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
         
        num_unread_messages = NotRead.objects.filter(user=request.user.id).count()    
            
        return Response({'messages' : num_unread_messages}  )
            
class GetAudioMessageView(APIView):
   
    authentication_classes = []#[authentication.TokenAuthentication]
    permission_classes = []#[IsAuthenticated]
    
    def get(self, request, audio_id):
         
        file = AudioMessage.objects.get(id=audio_id)
        
        return FileResponse(open(file.audio_data.path, 'rb'))
"""          
@require_http_methods(["POST"])   
def addConversation(request):
    
    jsonData = json.loads(request.POST['data'])
   
    topic = jsonData['topic']    
    target_id = jsonData['target']    
        
    target_user = User.objects.get(id=target_id)
    conversation = Conversation.objects.create(topic=topic)     
    
    conversation.participants.add(request.user)
    conversation.participants.add(target_user)
    
    #user_to_conversation = UserToConversation.objects.create(user=request.user, conversation=conversation)
    #target_user_to_conversation = UserToConversation.objects.create(user=target_user, conversation=conversation)
    
    conversation.save()
    #user_to_conversation.save()
    #target_user_to_conversation.save()
    
    return HttpResponse(status=200)  
"""
def updateMessage(conversation, message, sender):
    
    conversation.latest_message = message.id   
    conversation.save()
    
    for user in conversation.participants.all():
        if user != sender:
            not_read_flag = NotRead.objects.create(user=user, message=message)
            not_read_flag.save()
   
    message_json = message.to_json()
    message_json['new_message'] = True

    update_message_json = {
        'type': 'update',
        'update': 'message',
        'conversation': conversation.id ,
        'message': message_json
    }
    for user in conversation.participants.all():        
        if not RoomHandler.instance.send(user.id, update_message_json):
            new_notification = Notification.objects.create(user=user, 
                    notification_type=Notification.TYPE_NEW_MESSAGE, target_id=conversation.id, source_id=sender.id)
            new_notification.save()
            
    
    
"""
@require_http_methods(["POST"])   
def sendMessage(request):
    
    jsonData = json.loads(request.POST['data'])
   
    conversation_id = jsonData['conversation']
    message_text = jsonData['text']    
    
    conversation = Conversation.objects.get(id=conversation_id)     
    
    message = Message.objects.create(sender=request.user.id, text=message_text, conversation=conversation)
    message.save()
    
    updateMessage(conversation, message, request.user)
    
    return HttpResponse(status=200)
"""
"""
@require_http_methods(["POST"])   
def sendFileMessage(request):    
    
    conversation_id = request.POST['conversation']
    
    conversation = Conversation.objects.get(id=conversation_id)   
    
    file = createFileEntry(request)
    file.shared_users = FileShareUsers.objects.create()
   
    
    for other_user in conversation.participants.all():
        if other_user != request.user:
            file.shared_users.users.add(other_user)
    
    message = Message.objects.create(sender=request.user.id, text=file.name, 
        conversation=conversation, message_type=Message.TYPE_FILE, data_id=file.id)
    
    message.save()
    file.shared_users.save()  
    file.save()  
    
    updateMessage(conversation, message, request.user)
    
    return HttpResponse(status=200)

@require_http_methods(["POST"])   
def sendAudioMessage(request):
    
    conversation_id = request.POST['conversation']
    conversation = Conversation.objects.get(id=conversation_id)
    
    audio_message = AudioMessage.objects.create(audio_data=request.FILES['files'])
    audio_message.save();           
    
    message = Message.objects.create(sender=request.user.id, text='audio', 
        conversation=conversation, message_type=Message.TYPE_AUDIO, data_id=audio_message.id)
    message.save()
       
    updateMessage(conversation, message, request.user)
    
    return HttpResponse(status=200) 
"""
"""
def listConversations(request):    
    
    answer = {  
        'conversations' : []
    }
    
    for conversation in request.user.conversations.all():
         
        conversation_data = {
                'id' : conversation.id,
                'topic' : conversation.topic,
                'users' : [],
                'latest' : None
        }
        for user in conversation.participants.all():
            conversation_data['users'].append(user.id)
        
        if conversation.latest_message > -1:
            conversation_data['latest'] = Message.objects.get(id=conversation.latest_message).to_json()
            
        answer['conversations'].append(conversation_data)
    
           
    return JsonResponse(answer)


def requestConversation(request, conversation_id):    
    
    answer = {  
        'messages' : []
    }
    
    conversation = Conversation.objects.get(id=conversation_id) 
    
    for message in conversation.messages.all():
        
        message_data = message.to_json()
           
        has_read_flag = NotRead.objects.filter(message=message.id, user=request.user.id)
       
        if has_read_flag.exists() is not None:
            has_read_flag.delete()
       
        answer['messages'].append(message_data) 
            
    return JsonResponse(answer)
"""
 
def downloadAudioMessage(request, audio_id):
    
    file = AudioMessage.objects.get(id=audio_id)
        
    return FileResponse(open(file.audio_data.path, 'rb'))

"""
@require_http_methods(["POST"])   
def addConference(request):
    
    jsonData = json.loads(request.POST['data'])
   
    title = jsonData['title']
    planned_duration = int(jsonData['duration'])    
    start_date = jsonData['start']    
        
    conference = Conference.objects.create(
        creator=request.user,
        title=title,
        planned_duration=planned_duration, 
        start_date=parse_datetime(start_date)
        )
    conference.save()
        
    return HttpResponse(status=200) 
"""

def conferenceToJson(conference):
    
    conference_data = {
        'id' : conference.id,
        'title' : conference.title,
        'start' : str(conference.start_date),
        'creator': conference.creator_id,
        'duration': conference.planned_duration,
        'meeting_room': conference.meeting_room_id,
        'conversation': conversation_to_json(None, conference.conversation),
        'call': conference.call_id,
        'users': []
    }
    for user in conference.participants.all():
        conference_data['users'].append(user.id)  
    
    return conference_data;

def checkConferenceTimeFrame(start_date, end_date, meeting_room_id):

    found_conferences = Conference.objects.filter(        
        Q(start_date__lte=start_date, end_date__gte=start_date) | 
        Q(start_date__lte=end_date, end_date__gte=end_date),
        meeting_room=meeting_room_id)

    return found_conferences.count() == 0
    '''
    start = start_date.timestamp()
    end = end_date.timestamp()
    
    print('  %i -> %i ' % (start, end))
    for conference in Conference.objects.filter(meeting_room=meeting_room_id):
        target_start = conference.start_date.timestamp()
        target_end = conference.end_date.timestamp()
        
        
        print('> %i < %i > %i : %s' % (target_start,start, target_end, conference.title))
    
        if start >= target_start and start <= target_end:
            return False
        if end >= target_start and end <= target_end:
            return False       
    
    return True
    '''

class SaveConferenceView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        conference_id = request.data.get('id')
    
        title = request.data.get('title')
        planned_duration = int(request.data.get('duration'))    
        start_date = parse_datetime(request.data.get('start'))    
        users = request.data.get('users')   
        meeting_room_id = request.data.get('meeting_room')   
        
        
        try:
            
            start_date = start_date.replace(second=0, microsecond=0)
            end_date = datetime.fromtimestamp(start_date.timestamp() + (planned_duration * 60.0), start_date.tzinfo)
                    
            if conference_id == -1:
                if not checkConferenceTimeFrame(start_date, end_date, meeting_room_id):
                    
                    return Response(status=404)
            
            
            meeting_room = Room.objects.get(id=meeting_room_id)
            
            conference = None
            
            old_users = set()
            
            if conference_id == -1:  
                
                conference_conversation = Conversation.objects.create(topic=title, is_conference=True)
                conference_conversation.participants.add(request.user)
                
                conference = Conference.objects.create(
                    creator=request.user,
                    title=title,
                    planned_duration=planned_duration, 
                    start_date=start_date,
                    end_date=end_date,
                    meeting_room = meeting_room,
                    conversation = conference_conversation
                )
            
            else:
                conference = Conference.objects.get(id=conference_id)
                #conference.title = title
                #conference.planned_duration = planned_duration
                #conference.start_date = start_date
                #conference.end_date = end_date
                #conference.meeting_room = meeting_room
                
                for p in  conference.participants.all():
                    old_users.add(p.id)
                    
            
            new_users = []
            
            for u in users:
                if u != conference.creator_id and u not in old_users:
                    new_user = User.objects.get(id=u)
                    new_users.append(new_user)
                    conference.participants.add(new_user)  
                    conference.conversation.participants.add(new_user)  
               
            conference.conversation.save()  
            conference.save()
            
            new_conference_json = conferenceToJson(conference)
            update_message = {'type' : 'update', 'update': 'conference', 'conference':  new_conference_json}
            #Update all users about the new, or changed conference
            for u in users:
               
                RoomHandler.instance.send(u, update_message)
            
            #Notify new users that they were invited to a conference     
            for u in new_users:
                    
                new_notification = Notification.objects.create(user=u, notification_type=Notification.TYPE_CONFERENCE_INVITATION, target_id=conference.id)
                new_notification.save()
                
                update_json = {'type' : 'update', 'update': 'notification', 'notification':  new_notification.to_json()}
                
                if RoomHandler.instance.send(u.id,update_json ):
                    new_notification.has_read = True
                    new_notification.save()
                    
            return Response(new_conference_json)
        except:
            return Response(status=404)


"""
@require_http_methods(["POST"])   
def assignUserConference(request):
    
    jsonData = json.loads(request.POST['data'])
   
    conference_id = jsonData['conference']
    user_id = jsonData['user']
   
    conference = Conference.objects.get(id=conference_id)
    user = User.objects.get(id=user_id)
    
    conference.participants.add(user)
    conference.save()
    
    return HttpResponse(status=200) 
"""

class ListConferencesView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        answer = { }
            
        for conference in request.user.created_conferences.filter(end_date__gt=datetime.utcnow()):
        #for conference in request.user.created_conferences.all():
            answer[conference.id] = conferenceToJson(conference) 
    
            
        for conference in Conference.objects.filter(participants__id=request.user.id, end_date__gt=datetime.utcnow()):
            answer[conference.id] = conferenceToJson(conference) 
        
        return Response(answer)
    
class ListAllConferencesView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        answer = { }
                    
        for conference in Conference.objects.all():
            answer[conference.id] = conferenceToJson(conference) 
        
        return Response(answer)
        
class JoinConferencesView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):  
          
        return Response()
    
class LeaveConferencesView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):  
          
        return Response()
    
class CallUserView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        target_id = request.data.get('user')
        conversation_id = request.data.get('conversation')
        
        target_user = User.objects.get(id=target_id)
        conversation = Conversation.objects.get(id=conversation_id)
        
        call_id = CallHandler.instance.callUser(request.user, target_user, conversation)
    
        if call_id > -1:
            return Response({'called': True, 'call':call_id })
        
        return Response({'called': False})
    

class JoinCallView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        call_id = request.data.get('call')
        #call = CallId.objects.get(id=call_id)
        
        if not CallHandler.instance.joinCall(request.user.id, call_id):
            return Response(status=404)  
        
        return Response()
        
class DeclineCallView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        #target_id = request.data.get('user')
        call_id = request.data.get('call')
        call = CallId.objects.get(id=call_id)
        
        CallHandler.instance.declineCall(request.user.id, call)
       
        return Response() 
    
class LeaveCallView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        #target_id = request.data.get('user')
        
        CallHandler.instance.leaveCall(request.user.id)
       
        return Response() 
"""    
def listConferences(request):    
    
    if not request.user.is_authenticated:
        return HttpResponse(status=404)
    
    answer = { 'conferences' : {} }
    
    
    for conference in request.user.created_conferences.all():
        answer['conferences'][conference.id] = conferenceToJson(conference) 
    
            
    for conference in Conference.objects.filter(participants__id=request.user.id):
        answer['conferences'][conference.id] = conferenceToJson(conference) 
        
       
    return JsonResponse(answer)

def listAllConferences(request):    
    
    if not request.user.is_authenticated:
        return HttpResponse(status=404)     
     
    answer = { 'conferences' : [] }
    
    for conference in Conference.objects.all():
        conference_data = conferenceToJson(conference) 
       
        #answer[conference.id] = conference_data
        answer['conferences' ].append(conference_data)
        
       
    return JsonResponse(answer)
"""

def listAudioMessage(request):    
    
    answer = {  
        'messages' : []
    }
    
    
    for a in AudioMessage.objects.all():
        #print("-----------")
                
        data = {
            'id' : a.id,
            #'data': base64.encodebytes(bytes(a.audio_data)).decode('utf-8')
        }        
       
        answer['messages'].append(data)
    return JsonResponse(answer)

"""
@require_http_methods(["POST"])   
def joinCall(request):
    
    jsonData = json.loads(request.POST['data'])
   
    call_id = jsonData['call']  
    title = jsonData['title']  
    
    if call_id == -1:
        call = Call.objects.create(title=title)
    else:
        call = Call.objects.get(id=call_id)
        
    call.participants.add(request.user)
    call.save()
    
    CallHandler.instance.join(call.id, request.user.id)
    
    return HttpResponse(status=200)  
    
"""