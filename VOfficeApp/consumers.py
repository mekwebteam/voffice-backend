import logging

from channels.generic.websocket import JsonWebsocketConsumer
import urllib
from rest_framework.authtoken.models import Token

from VOfficeApp.room_handler import RoomHandler, CallHandler

from VOfficeApp.submodels.conversation import Message, NotRead
from VOfficeApp.submodels.userdata import Notification

logger = logging.getLogger(__name__)

class OfficeConsumer(JsonWebsocketConsumer):
    
    
    def connect(self):
                
        params = urllib.parse.parse_qs(self.scope['query_string'])
                
        try:
            token = Token.objects.get(key=params[b'token'][0].decode("utf-8"))
            
            self.scope["user"] = token.user
            self.accept()
                        
            RoomHandler.instance.registerUser(self.scope["user"].id, self)
            
        except:
            self.close()           
       
            
            
            
    def receive_json(self, content):
        if content['type'] == 'message':
            message_id = content['message'] 
            
            has_read_flag = NotRead.objects.filter(message=message_id, user=self.scope["user"].id)
           
            if has_read_flag.exists():
                has_read_flag.delete()
                
            #message = Message.objects.get(id=message_id)
            
            #user_message = message.usertomessage_set.get(user_conversation__user_id=self.scope["user"].id)
            #user_message.has_read = True
            #user_message.save()
            
        elif content['type'] == 'signal':
                                
            if content['signal'] == 'join':                
                call_id = content['call']                                                
                CallHandler.instance.joinCall(call_id, self.scope["user"], self)                
            elif(content["signal"] == "leave"):
                CallHandler.instance.leaveCall(self.scope["user"].id)                                                    
            elif(content["signal"] == "offer"):
                self.relaySignal(content["id"], 'offer', content["data"])                
            elif(content["signal"] == "answer"):
                self.relaySignal(content["id"], 'answer', content["data"])
            elif(content["signal"] == "ice"):
                self.relaySignal(content["id"], 'ice', content["data"])  
        
        
        elif content['type'] == 'notification': 
            #Mark notification as read 
            notification_id = content['notification']      
            
            notification = Notification.objects.get(id=notification_id)                       
            notification.has_read = True
            notification.save()
            
    def relaySignal(self, target_user_id, signal_type, data):                 
                          
        message = {
            "type": "signal", 
            "signal":signal_type, 
            "id": self.scope["user"].id,
            "data": data
        }
        
        RoomHandler.instance.consumers[target_user_id].send_json(message)    
        
        #print(signal_type)
    
    def disconnect(self, code):

        #CallHandler.instance.leaveCall(self.scope["user"].id)
        RoomHandler.instance.unregisterUser(self.scope["user"].id)
   
    
