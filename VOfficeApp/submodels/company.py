from django.db import models

class HierarchyLevel(models.Model):
    name = models.CharField(max_length=128, default="")
    
    class Meta:
        abstract = True

    def __str__(self):
        return '%s(%i)' % (self.name, self.id)

    
class Company(HierarchyLevel):    
    """
    Represents a company.
    Has child branches, and child group companies
    """
    pass    

class GroupCompany(HierarchyLevel):    
    """
    Represents a group company, behaves similiar as a company but only has child branches
    """
    company = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True)
        
class Branch(HierarchyLevel):   
    """
    A branch of a company with child departments.
    """ 
    company = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True, blank=True)
    group_company = models.ForeignKey(GroupCompany, on_delete=models.SET_NULL, null=True, blank=True)
    
class Department(HierarchyLevel): 
    """
    Represents a department.
    """   
    branch = models.ForeignKey(Branch, on_delete=models.SET_NULL, null=True)       
    
    room = models.OneToOneField('Room', on_delete=models.SET_NULL, blank=True, null=True, help_text="The visual room of this department")
    

class Team(models.Model):
    """
    Stores all team data
    """
    name = models.CharField(max_length=128, default="")
    
    creator = models.ForeignKey('User',related_name='created_teams', on_delete=models.SET_NULL, null=True, help_text="The creator of this Team, only the creator can modify it")   
    members = models.ManyToManyField('User', help_text="All other members of this team, excpet the creator")
    
    

class EmployeeAssignment(models.Model):
    """
    An employee can be assigned to a depertment, branch or company.
    If the employee is assigned to a branch or company, an office room is created(Not used at the moment!)
    """
    
    department = models.ForeignKey(Department, on_delete=models.SET_NULL, null=True, blank=True)         
    branch = models.ForeignKey(Branch, on_delete=models.SET_NULL, null=True, blank=True)    
    group_company = models.ForeignKey(GroupCompany, on_delete=models.SET_NULL, null=True, blank=True)        
    company = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True, blank=True)
    
    room = models.OneToOneField('Room', on_delete=models.SET_NULL, blank=True, null=True, help_text="Only for employees with their own office room")
    workspace = models.IntegerField(default=-1, help_text="Index of the workspace in the department room")
    department_head = models.BooleanField(default=False)
        
    def __str__(self):
        
        try:
        
            assigned = "-"
            if self.company is not None:
                assigned = self.company.name
            elif self.group_company_mapping is not None:
                assigned = self.group_company.name
            elif self.branch_mapping is not None:
                assigned = self.branch.name
            elif self.department_mapping is not None:
                assigned = self.department.name
                
            return '%s' % (assigned) #self.employeedata.userdata.user.email
        except Exception:
            return 'EmployeeAssignment'
        
        