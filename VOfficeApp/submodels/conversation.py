from django.db import models

class Message(models.Model):
    """
    A message from a Conversation.
    """
    TYPE_TEXT = 't'
    TYPE_AUDIO = 'a'
    TYPE_FILE = 'f'
    TYPE_CALL = 'c'
    
    MESSAGE_TYPE_CHOICES = (
        (TYPE_TEXT, 'text'),
        (TYPE_AUDIO, 'audio'),
        (TYPE_FILE, 'file'),
        (TYPE_CALL, 'call'),
    )
    
    sender = models.IntegerField(help_text="User id of the sender, not a foreign key for performance reasons")
    text = models.TextField(help_text="Message Text")
    date = models.DateTimeField(auto_now_add=True)
    
    conversation = models.ForeignKey('Conversation', related_name='messages', on_delete=models.CASCADE)
    
    #points to file or audio file
    data_id = models.BigIntegerField(default=-1, help_text="Not used for text messages, audio ID for audio messages, file ID for file messages, call ID for call messages")
    
    message_type = models.CharField(
        max_length=2,
        choices=MESSAGE_TYPE_CHOICES,
        default=TYPE_TEXT,
    )
    
    class Meta:
        indexes = [
            models.Index(fields=['conversation', 'conversation']),
        ]
        
    def to_json(self):
        data = {
           'id' : self.id, 
           'conversation': self.conversation_id,
           'sender' : self.sender, 
           'type': self.message_type,
           'date' : str(self.date), 
           'text' : self.text, 
           'data_id': self.data_id,
           'new_message': False,
           'call': None
        }
        if self.message_type == Message.TYPE_CALL:
            
            try:
                
                if not self.call is None:
                                                
                    data['call'] = {
                        'id': self.call.id,
                        'established': self.call.was_active,
                        'end_date' : str(self.call.end_date)
                    }
                else:
                    data['call'] = {
                        'established': False,
                    }
            except:
                #print("except")
                data['call'] = {
                    'established': False,
                }   
            
        return data



class NotRead(models.Model): 
    """
    Maps which user has read which message
    """
    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    
    class Meta:
        indexes = [
            models.Index(fields=['user', 'user']),
            models.Index(fields=['message', 'message']),
        ]
        
    
"""
class UserToMessage(models.Model):
    
    message = models.ForeignKey('Message', on_delete=models.CASCADE)
    user_conversation = models.ForeignKey('UserToConversation', on_delete=models.CASCADE)
    
    has_read = models.BooleanField(default=False)

class UserToConversation(models.Model):
    
    user = models.ForeignKey('User', related_name='conversations', on_delete=models.SET_NULL, null=True)
    conversation = models.ForeignKey('Conversation', related_name='users', on_delete=models.SET_NULL, null=True)
    
    messages = models.ManyToManyField(Message, through='UserToMessage')
"""
    
class Conversation(models.Model):    
    """
    A conversation between two or more Users
    """
    participants = models.ManyToManyField('User', related_name='conversations')   
    
    topic = models.CharField(max_length=1024)
    latest_message = models.IntegerField(default=-1, help_text="Message ID of the latest message, not a foreign key for performance reasons.")
    
    is_conference = models.BooleanField(default=False, help_text="If this Conversation is part of a conference")

def upload_path(instance, filename):

    return 'audios/{0}'.format(filename)
#class AudioMessage(models.Model):

#    audio_data = models.FileField(upload_to=upload_path)
    
    #audio_data = models.BinaryField()
    
class CallId(models.Model):
    """
    Data about calls
    """
    end_date = models.DateTimeField(auto_now_add=True , null=True)
    
    message = models.OneToOneField('Message', related_name='call', on_delete=models.SET_NULL, null=True, blank=True)
    was_active = models.BooleanField(default=False)

def newCallId():
    return CallId.objects.create()

class Conference(models.Model):
    """
    A planned Conference.
    """ 
    class Meta:
        indexes = [
            models.Index(fields=['archived', 'archived']),
    ]
        
    participants = models.ManyToManyField('User', related_name='conferences')
    
    title = models.CharField(max_length=1024)
       
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(help_text="Planned end date, used for scheduling meeting rooms")
    planned_duration = models.IntegerField()
        
    conversation = models.OneToOneField('Conversation', related_name='conference', on_delete=models.SET_NULL, null=True)
    creator = models.ForeignKey('User', related_name='created_conferences', on_delete=models.SET_NULL, null=True)
    
    meeting_room = models.ForeignKey('Room', related_name='conferences', on_delete=models.SET_NULL, null=True)
    
    call = models.ForeignKey(CallId, on_delete=models.SET_NULL, default=newCallId, null=True)
    #end_date = models.DateTimeField(blank=True, null=True)
    archived = models.BooleanField(default=False)

'''
class Call(models.Model):
   
        
    participants = models.ManyToManyField('User', related_name='calls')
    #Conversation
    title = models.CharField(max_length=1024)
       
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(auto_now_add=True)
    
    archived = models.BooleanField(default=False)
    
'''