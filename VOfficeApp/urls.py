from django.urls import path
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.views import LoginView

from django.conf import settings
from django.conf.urls import include

from . import views
from VOfficeApp.subviews.user import *
from VOfficeApp.subviews.company import *
from VOfficeApp.subviews.office import *
from VOfficeApp.subviews.files import *
from VOfficeApp.subviews.conversation import *

from django.views.generic import TemplateView


from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
   url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
   url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
   
    path('test/<slug:page>', views.testView, name='testView'), 
    
    path('createTestUsers', createTestUsers, name='createTestUsers'), 
    path('createTestCompany', createTestCompany, name='createTestCompany'),
    path('createTestRooms', createTestRooms, name='createTestRooms'),
    path('loadCompanyData', loadCompanyData, name='loadCompanyData'),
    path('listUsers/<slug:user_type_param>', listUsers, name='listUsers'), 
    path('uploadProfileImage', uploadProfileImage, name='uploadProfileImage'), 
    path('setProfilePictures', setProfilePictures, name='setProfilePictures'), 
    
    
    #path('listNotAssignEmployees', listNotAssignEmployees, name='listNotAssignEmployees'),   
    #path('listLevels', listLevels, name='listLevels'),     
    #path('assignUser', assignUser, name='assignUser'),   
    
]    
"""
    path('', views.indexView, name='indexView'),  
    path('login', views.indexView, name='indexView'),  
    path('conferences', views.indexView, name='indexView'),  
     
    
    path('voffice/login', views.userLogin, name='userLogin'),    
    path('voffice/getUser', views.getUser, name='getUser'),    
    
    
    
    #path('login/', LoginView.as_view(template_name='VOfficeApp/login.html'), name='login'),
    
    path('voffice/logout', views.logoutView, name='logoutView'),    
    
    path('addUser', addUser, name='addUser'),      
       
    path('listUsers/<slug:user_type_param>', listUsers, name='listUsers'),     
    
    path('addLevel', addLevel, name='addLevel'),   
          
    path('addRoomTemplate', addRoomTemplate, name='addRoomTemplate'),       
    path('updateRoomTemplate', updateRoomTemplate, name='updateRoomTemplate'),       
    path('listRoomTemplates', listRoomTemplates, name='listRoomTemplates'),       
    path('assignRoomTemplate', assignRoomTemplate, name='assignRoomTemplate'),       
    path('addRoom', addRoom, name='addRoom'),      
     
    path('getPermissionData', getPermissionData, name='getPermissionData'),       
    path('addRole', addRole, name='addRole'),       
    path('assignRolePermission', assignRolePermission, name='assignRolePermission'),       
    path('removeRolePermission', removeRolePermission, name='removeRolePermission'),       
    path('addDesignation', addDesignation, name='addDesignation'),       
    path('assignDesignationRole', assignDesignationRole, name='assignDesignationRole'),       
    path('removeDesignationRole', removeDesignationRole, name='removeDesignationRole'),       
    path('assignUserDesignation', assignUserDesignation, name='assignUserDesignation'),       
    
    path('getUserInformation/<int:user_id>', getUserInformation, name='getUserInformation'), 
    path('setUserInformation', setUserInformation, name='setUserInformation'), 
    path('getProfileImage/<int:id>', getProfileImage, name='getProfileImage'), 
    path('uploadProfileImage', uploadProfileImage, name='uploadProfileImage'), 
    
    path('getOffice', getOffice, name='getOffice'), 
    path('getOfficeStatus', getOfficeStatus, name='getOfficeStatus'), 
    path('enterRoom', enterRoom, name='enterRoom'), 
    path('enterOffice', enterOffice, name='enterOffice'), 
    
    path('addConversation', addConversation, name='addConversation'), 
    path('sendMessage', sendMessage, name='sendMessage'), 
    path('listConversations', listConversations, name='listConversations'), 
    path('requestConversation/<int:conversation_id>', requestConversation, name='requestConversation'), 
    path('downloadAudioMessage/<int:audio_id>', downloadAudioMessage, name='downloadAudioMessage'), 
    path('getMessageStatus', getMessageStatus, name='getMessageStatus'), 
    path('listCalls', listCalls, name='listCalls'), 
    path('sendAudioMessage', sendAudioMessage, name='sendAudioMessage'), 
    path('sendFileMessage', sendFileMessage, name='sendFileMessage'), 
    
    path('voffice/saveConference', saveConference, name='saveConference'), 
    path('voffice/listConferences', listConferences, name='listConferences'), 
    path('voffice/listAllConferences', listAllConferences, name='listAllConferences'), 
    #path('assignUserConference', assignUserConference, name='assignUserConference'), 
    
    path('listAudioMessage', listAudioMessage, name='listAudioMessage'), 
         
        
    
    path('uploadFile', uploadFile, name='uploadFile'), 
    path('addFolder', addFolder, name='addFolder'), 
    path('listFolders', listFolders, name='listFolders'), 
    path('listFiles', listFiles, name='listFiles'), 
    path('listSharedFiles', listSharedFiles, name='listSharedFiles'), 
    path('shareFileLevel', shareFileLevel, name='shareFileLevel'), 
    path('downloadFile/<int:file_id>', downloadFile, name='downloadFile'),
    ]
    
"""

